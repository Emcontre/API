package ascendlearning.data;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ascendlearning.models.PackagePermission;

public class PackagePermissionData {

	@SuppressWarnings("unchecked")
	public static JSONObject patchPermissionToPackage(int id, String name, String type) {
		JSONArray arrayPermissions = new JSONArray();
		JSONObject jsonPermission1 = new JSONObject();
		jsonPermission1.put(CommonData.PERMISSIONTYPE, type);
		jsonPermission1.put(CommonData.DURATIONINDAYS, 5);
		jsonPermission1.put(CommonData.ID, id);
		jsonPermission1.put(CommonData.NAME, name);
		arrayPermissions.add(jsonPermission1);
		JSONObject jsonPostBody = new JSONObject();
		jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
		return (jsonPostBody);
	}

	public static HashMap<String, Object> updatePermission() {
		PackagePermission packagePermission = new PackagePermission();
		packagePermission.setDurationInDays(365);
		HashMap<String, Object> map = new HashMap<>();
		map.put(CommonData.DURATIONINDAYS, packagePermission.getDurationInDays());
		return map;
	}

	public static HashMap<String, String> permissionWithInvalidIntegerParameter() {
		PackagePermission packagePermission = new PackagePermission();
		packagePermission.setInvalidDurationInDays("60.5");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.DURATIONINDAYS, packagePermission.getInvalidDurationInDays());
		return map;
	}

	@SuppressWarnings("unchecked")
	public static JSONObject assignPermissionToPackageWithInvalidInteger(int id, String name, String type) {
		JSONArray arrayPermissions = new JSONArray();
		JSONObject jsonPermission1 = new JSONObject();
		jsonPermission1.put(CommonData.DURATIONINDAYS, "1.6");
		jsonPermission1.put(CommonData.ID, id);
		jsonPermission1.put(CommonData.PERMISSIONTYPE, type);
		jsonPermission1.put(CommonData.NAME, name);
		arrayPermissions.add(jsonPermission1);
		JSONObject jsonPostBody = new JSONObject();
		jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
		return (jsonPostBody);
	}
}
