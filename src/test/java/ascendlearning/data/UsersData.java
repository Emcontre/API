package ascendlearning.data;

import ascendlearning.models.User;
import com.github.javafaker.Faker;

import java.util.HashMap;

public class UsersData {

	private static final String PHONE_NUMBER_FORMAT = "\\(\\d\\d\\d\\)\\d\\d\\d-\\d\\d\\d\\d";

	/**
	 * Creates a new {@link User} with random data, set to active
	 * 
	 * @return a random active user Converts a {@link User} into a HashMap to send
	 *         as the body in a request to create new users
	 * @param user
	 *            the user to be converted into a Hashmap
	 * @return the Hashmap mapping the user fields to the correct key
	 */

	public static HashMap<String, String> randomActiveUser() {
		Faker faker = new Faker();
		User user = new User();
		user.setFirstName(faker.name().firstName());
		user.setLastName(faker.name().lastName());
		user.setUsername(user.getFirstName() + user.getLastName() + faker.number().digit());
		user.setEmailAddress(faker.internet().emailAddress());
		user.setPassword(faker.internet().password(8, 12, true, true) + "Aa!");
		user.setMobileNumber(faker.regexify(PHONE_NUMBER_FORMAT));
		user.setPhoneNumber(faker.regexify(PHONE_NUMBER_FORMAT));
		user.setActive(true);
		user.setSuspended(false);
		HashMap<String, String> map = new HashMap<>();
		map.put("first_name", user.getFirstName());
		map.put("last_name", user.getLastName());
		map.put("username", user.getUsername());
		map.put("email", user.getEmailAddress());
		map.put(CommonData.PSSWRDSTRING, user.getPassword());
		map.put(CommonData.PSSWRDCONFIRMATIONSTRING, user.getPassword());
		map.put("preferred_language", "en-US");
		map.put("suspended", Boolean.toString(user.isSuspended()));
		map.put("active", Boolean.toString(user.isActive()));
		map.put("phone_number", user.getPhoneNumber());
		map.put("mobile_phone", user.getMobileNumber());
		return map;
	}
	
	public static HashMap<String, String> randomNewFirstAndLastNameForUser() {
		Faker faker = new Faker();
		User user = new User();
		user.setFirstName(faker.name().firstName());
		user.setLastName(faker.name().lastName());
		HashMap<String, String> map = new HashMap<>();
		map.put("first_name", user.getFirstName());
		map.put("last_name", user.getLastName());
		return map;
	}
	
	public static HashMap<String, String> updateInvalidPassword() {
		User user = new User();
		user.setConfirmPassword("");
		user.setPassword("");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.PSSWRDSTRING, user.getPassword());
		map.put("confirm_password", user.getConfirmPassword());
		return map;		
	}
	
	public static HashMap<String, Object> suspendExistingUser(int id) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("id", id);
		map.put("suspension_reason", "valid_suspension_reason");
		return map;		
	}
	
	public static HashMap<String, Object> reinstateExistingUser(int id) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("id", id);
		return map;		
	}
	
	public static HashMap<String, Object> updateUserData() {
		User user = new User();
		user.setStreetAddress1("test street11111");
		user.setStreetAddress2("test street22111");
		user.setCity("test city11");
		user.setLocality("test locality1111");
		user.setProvince("test state11");
		user.setPostalCode("postal_code121");
		user.setCountryCode("in");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.STREETADDRESS1, user.getStreetAddress1());
		map.put("street_address2", user.getStreetAddress2());
		map.put(CommonData.CITY, user.getCity());
		map.put("locality", user.getLocality());
		map.put(CommonData.PROVINCE, user.getProvince());
		map.put(CommonData.POSTALCODE, user.getPostalCode());
		map.put(CommonData.COUNTRYCODE, user.getCountryCode());
		HashMap<String, Object> finalMap = new HashMap<>();
		finalMap.put("street_address_foo", map);
		return finalMap;
	}
	
	public static HashMap<String, String> toGetPasswordChangeToken() {
		HashMap<String, String> map = new HashMap<>();
		map.put("last_service_url", "http://localhost");
		return map;
	}
	
	public static HashMap<String, String> passwordChangeData(String token) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("token", token);
		map.put(CommonData.PSSWRDSTRING, CommonData.PSSWRD);
		map.put(CommonData.PSSWRDCONFIRMATIONSTRING, CommonData.CONFIRMPSSWRD);
		map.put("password_reset_question",CommonData.NEWQUESTION);
		map.put("password_reset",CommonData.NEWQUESTION);
		return map;
	}
	
	public static HashMap<String, String> invalidPasswordChangeData(String token) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("token", token);
		map.put(CommonData.PSSWRDSTRING, null);
		map.put(CommonData.PSSWRDCONFIRMATIONSTRING, null);
		map.put("password_reset_question",CommonData.NEWQUESTION);
		map.put("password_reset",CommonData.NEWQUESTION);
		return map;
	}
	
	public static HashMap<String, String> invalidSuspensionUserData(int id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(CommonData.ID, Integer.toString(id));
        map.put("suspension_reason", null);
        return map;
    }
}
