package ascendlearning.data;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.github.javafaker.Faker;

import ascendlearning.models.Permissions;

public class PermissionsData {

	@SuppressWarnings("unchecked")
	public static JSONObject validPermissionInformation() {
		Faker faker = new Faker();
		JSONArray arrayPermissions = new JSONArray();
		JSONObject jsonPermission1 = new JSONObject();
		jsonPermission1.put(CommonData.PERMISSIONTYPE, faker.letterify("?????????????????????????"));
		jsonPermission1.put(CommonData.PERMISSIONDURATION, 60);
		jsonPermission1.put(CommonData.NAME, faker.letterify("?????????????????????????"));
		arrayPermissions.add(jsonPermission1);
		JSONObject jsonPostBody = new JSONObject();
		jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
		return(jsonPostBody);
	}
	
	public static HashMap<String, String> updatePermissionDetails() {
		Permissions permission = new Permissions();
		Faker faker = new Faker();
		permission.setPermissionType(faker.letterify("???????????????"));
        HashMap<String, String> map = new HashMap<>();
        map.put(CommonData.PERMISSIONTYPE, permission.getPermissionType());
        return map;
    }
	
	public static HashMap<String, String> permissionDetails() {
        Faker faker = new Faker();
        Permissions permission = new Permissions();
        permission.setName(faker.letterify("?????????????????????????"));
        permission.setPermissionType(faker.letterify("?????????????????????????"));
        HashMap<String, String> map = new HashMap<>();
        map.put(CommonData.PERMISSIONNAME, permission.getName());
        map.put(CommonData.PERMISSIONTYPE, permission.getPermissionType());
        return map;
    }
	
	public static HashMap<String, String> updatePermissionDetailsByPermissionId() {
		Permissions st = new Permissions();
		Faker faker = new Faker();
		st.setPermissionType(faker.letterify("???????????????"));
		st.setPermissionName(faker.letterify("???????????????"));
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.PERMISSIONTYPE, st.getPermissionType());
		map.put(CommonData.PERMISSIONNAME, st.getPermissionName());
		return map;
	}
	
	public static HashMap<String, String> updateInvalidPermission() {
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.EXPIRATIONAT, "2016-04-12");
		return map;
	}	
}