package ascendlearning.data;

public class CommonData {
	public static final int INVALIDUSERID = 999999999;
	public static final int INVALIDSITELICENSEID = 999999999;
	public static final int PERMISSIONID = 999999999;
	public static final int INVALIDPERMISSIONID = 999999999;
	public static final String EXPIRYDATE = "2017-01-01 00:00:00";
	public static final int INVALIDPACKAGEID = 999999999;
	public static final String PSSWRD = "P@ssword@123";
	public static final String CONFIRMPSSWRD = "P@ssword@123";
	public static final String VALIDCITY = "Burlington";
	public static final String POSTALCODEVALUE = "01803";
	public static final String SAMPLEPERMISSIONNAME = "JenkinsTestPermissionName";
	public static final String SAMPLEPERMISSIONTYPE = "PermissionJenkinsType";
	public static final String NAME = "name";
	public static final String DURATIONINDAYS = "duration_in_days";
	public static final String PERMISSIONS = "permissions";
	public static final String PERMISSIONTYPE = "permission_type";
	public static final String PERMISSIONNAME = "permission_name";
	public static final String ID = "id";
	public static final String TESTAPIAUTOMATIONPACKAGE = "TestAPIAutomationPackage";
	public static final String PERMISSIONDURATION = "permission_duration";
	public static final String EXPIRATIONAT = "expiration_at";
	public static final String STREETADDRESS1 = "street_address1";
	public static final String CITY = "city";
	public static final String PROVINCE = "province";
	public static final String POSTALCODE = "postal_code";
	public static final String COUNTRYCODE = "country_code";
	public static final String RANDOMSTRING = "abchdefg";
	public static final String PSSWRDSTRING = "password";
	public static final String PSSWRDCONFIRMATIONSTRING = "password_confirmation";
	public static final String NEWQUESTION = "new question";
}