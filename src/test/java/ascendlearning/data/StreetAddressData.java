package ascendlearning.data;

import ascendlearning.models.StreetAddress;

import java.util.HashMap;

public class StreetAddressData {

	/**
	 * Converts a valid {@link StreetAdress} into a HashMap to send as the body in a
	 * request to create new streetaddress
	 *
	 * @param street
	 *            address the street address to be converted into a Hashmap
	 * @return the Hashmap mapping the street address fields to the correct key
	 */
	public static HashMap<String, String> validStreetAddressDetails() {

		StreetAddress st = new StreetAddress();
		st.setStreetAddress("185 Berry Street");
		st.setCity("San Francisco");
		st.setProvince("CA");
		st.setPostalCode("94107");
		st.setCountryCode("us");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.STREETADDRESS1, st.getStreetAddress());
		map.put(CommonData.CITY, st.getCity());
		map.put(CommonData.PROVINCE, st.getProvince());
		map.put(CommonData.POSTALCODE, st.getPostalCode());
		map.put(CommonData.COUNTRYCODE, st.getCountryCode());
		return map;

	}

	public static HashMap<String, String> validStreetAddressWithPostalCode() {

		StreetAddress st = new StreetAddress();
		st.setStreetAddress("11 Gately Drive");
		st.setPostalCode("12345");
		st.setCountryCode("us");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.STREETADDRESS1, st.getStreetAddress());
		map.put(CommonData.POSTALCODE, st.getPostalCode());
		map.put(CommonData.COUNTRYCODE, st.getCountryCode());
		return map;
	}

	public static HashMap<String, String> streetAddressWithInvalidPostalCode() {

		StreetAddress st = new StreetAddress();
		st.setStreetAddress(CommonData.RANDOMSTRING);
		st.setCity(CommonData.RANDOMSTRING);
		st.setProvince(CommonData.RANDOMSTRING);
		st.setPostalCode(CommonData.RANDOMSTRING);
		st.setCountryCode("us");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.STREETADDRESS1, st.getStreetAddress());
		map.put(CommonData.CITY, st.getCity());
		map.put(CommonData.PROVINCE, st.getProvince());
		map.put(CommonData.POSTALCODE, st.getPostalCode());
		map.put(CommonData.COUNTRYCODE, st.getCountryCode());
		return map;
	}
	
	public static HashMap<String, String> streetAddressWithInvalidCountryCode() {
		StreetAddress st = new StreetAddress();
        st.setStreetAddress("some address");
        st.setCity(CommonData.VALIDCITY);
        st.setProvince("MA");
        st.setPostalCode(CommonData.POSTALCODEVALUE);
        st.setCountryCode("invalid");
        HashMap<String, String> map = new HashMap<>();
        map.put(CommonData.STREETADDRESS1, st.getStreetAddress());
        map.put(CommonData.CITY, st.getCity());
        map.put(CommonData.PROVINCE, st.getProvince());
        map.put(CommonData.POSTALCODE, st.getPostalCode());
        map.put(CommonData.COUNTRYCODE, st.getCountryCode());
        return map;
    }
	
	public static HashMap<String, String> streetAddressWithOutCountryCode() {		 
        StreetAddress st = new StreetAddress();
        st.setStreetAddress("some address");
        st.setCity(CommonData.VALIDCITY);
        st.setProvince("MA");
        st.setPostalCode("01803");
        HashMap<String, String> map = new HashMap<>();
        map.put(CommonData.STREETADDRESS1, st.getStreetAddress());
        map.put(CommonData.CITY, st.getCity());
        map.put(CommonData.PROVINCE, st.getProvince());
        map.put(CommonData.POSTALCODE, st.getPostalCode());
        return map;
    }
	
	public static HashMap<String, String> streetAddressDetailsWithoutStreetAddress1() {
		StreetAddress st = new StreetAddress();
		st.setStreetAddress("");
		st.setCity("Burlington");
		st.setProvince("MA");
		st.setPostalCode("01803");
		st.setCountryCode("us");
		HashMap<String, String> map = new HashMap<>();
		map.put("street_address1", st.getStreetAddress());
		map.put("city", st.getCity());
		map.put("province", st.getProvince());
		map.put("postal_code", st.getPostalCode());
		map.put("country_code", st.getCountryCode());
		return map;
	}
}
