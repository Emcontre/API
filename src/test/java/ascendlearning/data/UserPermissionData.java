package ascendlearning.data;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.github.javafaker.Faker;

import ascendlearning.models.UserPermission;

public class UserPermissionData {
   
    @SuppressWarnings("unchecked")
	public static JSONObject invalidPermissionDetails() {
		JSONArray arrayPermissions = new JSONArray();
		JSONObject jsonPermission1 = new JSONObject();
		jsonPermission1.put(CommonData.EXPIRATIONAT,"2016-04-12");
		arrayPermissions.add(jsonPermission1);
		JSONObject jsonPostBody = new JSONObject();
		jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
		return(jsonPostBody);		
	}
    
    @SuppressWarnings("unchecked")
	public static JSONObject updatePermissionwithInvalidDuration(int id) {
        JSONArray arrayPermissions = new JSONArray();
        JSONObject jsonPermission1 = new JSONObject();
        jsonPermission1.put(CommonData.ID, id);
        jsonPermission1.put(CommonData.EXPIRATIONAT, "test invalid date");
        arrayPermissions.add(jsonPermission1);
        JSONObject jsonPostBody = new JSONObject();
        jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
        return(jsonPostBody);
    }
    
	public static HashMap<String, String> patchPermissionToUser() {
		UserPermission userpermission = new UserPermission();
		userpermission.setExpirationAt("2016-04-12");
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.EXPIRATIONAT, userpermission.getExpirationAt());
		return map;
	}
	
	@SuppressWarnings("unchecked")
    public static JSONObject assignPermissionToUser(int id,String name, String type) {
        JSONArray arrayPermissions = new JSONArray();
        JSONObject jsonPermission1 = new JSONObject();
        jsonPermission1.put(CommonData.PERMISSIONTYPE, type);
        jsonPermission1.put(CommonData.DURATIONINDAYS, 5);
        jsonPermission1.put(CommonData.ID, id);
        jsonPermission1.put(CommonData.NAME, name);
        arrayPermissions.add(jsonPermission1);
        JSONObject jsonPostBody = new JSONObject();
        jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
        return(jsonPostBody);
    }
	
	@SuppressWarnings("unchecked")
	public static JSONObject invalidPermissionId() {
		JSONArray arrayPermissions = new JSONArray();
		JSONObject jsonPermission1 = new JSONObject();
		jsonPermission1.put(CommonData.ID, CommonData.INVALIDPERMISSIONID);
		arrayPermissions.add(jsonPermission1);
		JSONObject jsonPostBody = new JSONObject();
		jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
		return(jsonPostBody);		
	}
	
	@SuppressWarnings("unchecked")
	public static JSONObject validPermissionInformationWithInvalidExpiryDate(int id, String name, String type) {
		JSONArray arrayPermissions = new JSONArray();
        JSONObject jsonPermission1 = new JSONObject();
        jsonPermission1.put(CommonData.PERMISSIONTYPE, type);
        jsonPermission1.put(CommonData.DURATIONINDAYS, 5);
        jsonPermission1.put(CommonData.ID, id);
        jsonPermission1.put(CommonData.NAME, name);
        jsonPermission1.put(CommonData.EXPIRATIONAT, CommonData.EXPIRYDATE);
        arrayPermissions.add(jsonPermission1);
        JSONObject jsonPostBody = new JSONObject();
        jsonPostBody.put(CommonData.PERMISSIONS, arrayPermissions);
        return(jsonPostBody);
	}
	
	@SuppressWarnings("unchecked")
    public static JSONObject invalidPermissionArray() {
		Faker faker = new Faker();
        JSONObject jsonPermission1 = new JSONObject();
        jsonPermission1.put(CommonData.NAME, faker.letterify("?????????????????????????"));
        jsonPermission1.put(CommonData.DURATIONINDAYS, 60);
        jsonPermission1.put(CommonData.EXPIRATIONAT, "2016-01-01");
        JSONObject jsonPostBody = new JSONObject();
        jsonPostBody.put(CommonData.PERMISSIONS, jsonPermission1);
        return(jsonPostBody);       
    }

	public static HashMap<String, String> invalidExpiryDataForPermissionUpdate() {
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.DURATIONINDAYS, "10");
		map.put("start_date", "2018-06-05");
		map.put(CommonData.EXPIRATIONAT, "2018-06-10");
		return map;
	}
}