package ascendlearning.data;

import ascendlearning.models.SiteLicense;

import java.util.HashMap;

public class SiteLicenseData {

	/**
	 * Converts a valid {@link SiteLicense} into a HashMap to send as the body in a
	 * request to create SiteLicense
	 *
	 * @param street
	 *            address the street address to be converted into a Hashmap
	 * @return the Hashmap mapping the street address fields to the correct key
	 */
	public static HashMap<String, String> validSiteLicenseData() {
		SiteLicense siteLicense = new SiteLicense();
		siteLicense.setIdInstitution("test1-institution");
		HashMap<String, String> map = new HashMap<>();
		map.put("id_for_institution", siteLicense.getIdInstitution());
		return map;
	}

	public static HashMap<String, String> updateRedemptionLimit() {
		SiteLicense siteLicense = new SiteLicense();
		siteLicense.setRedemptionLimit("12");
		HashMap<String, String> map = new HashMap<>();
		map.put("redemption_limit", siteLicense.getRedemptionLimit());
		return map;
	}

}