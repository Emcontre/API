package ascendlearning.data;

import ascendlearning.models.Package;

import java.util.HashMap;

import com.github.javafaker.Faker;

public class PackageData {

	/**
	 * Converts a valid {@link Package} into a HashMap to send as the body in a
	 * request to create package
	 *
	 * @param package
	 *            the package to be converted into a Hashmap
	 * @return the Hashmap mapping the package fields to the correct key
	 */
	public static HashMap<String, String> randomPackage() {
		Package pack = new Package();
		Faker faker = new Faker();
		String id = faker.number().digits(5);
		pack.setName(CommonData.TESTAPIAUTOMATIONPACKAGE + id);
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.NAME, pack.getName());
		return map;
	}
		
	public static HashMap<String, String> packageDetailsWithBlankParameters() {
		Package pack = new Package();
		pack.setName(null);
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.NAME, pack.getName());
		return map;
	}
	
	public static HashMap<String, String> packageWithSameName(String name) {
		Package pack = new Package();
		pack.setName(name);
		HashMap<String, String> map = new HashMap<>();
		map.put(CommonData.NAME, pack.getName());
		return map;
	}
}
