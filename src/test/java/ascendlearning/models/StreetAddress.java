package ascendlearning.models;

public class StreetAddress {
	
    private String streetaddress1;
    private String city;
    private String province;
    private String postalcode;
    private String countrycode;
    
    public String getStreetAddress() {
        return streetaddress1;
    }

    public void setStreetAddress(String streetaddress1) {
        this.streetaddress1 = streetaddress1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }
    
    public void setPostalCode(String postalcode) {
        this.postalcode = postalcode;
    }

    public String getPostalCode() {
        return postalcode;
    }

    public void setCountryCode(String countrycode) {
        this.countrycode = countrycode;
    }
    public String getCountryCode() {
        return countrycode;
    }
}
