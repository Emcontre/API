package ascendlearning.models;

public class SiteLicense {
	private String idforinstitution;
	private String redemptionLimit;

	public String getIdInstitution() {
		return idforinstitution;
	}

	public void setIdInstitution(String idforinstitution) {
		this.idforinstitution = idforinstitution;
	}

	public String getRedemptionLimit() {
		return redemptionLimit;
	}

	public void setRedemptionLimit(String redemptionLimit) {
		this.redemptionLimit = redemptionLimit;
	}
}