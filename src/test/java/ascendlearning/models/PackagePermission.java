package ascendlearning.models;

public class PackagePermission {
	private String permissiontype;
	private String name;
	private int id;
	private int durationInDays;
	private String invalidDurationInDays;

	public String getPermissionType() {
		return permissiontype;
	}

	public void setPermissionType(String permissiontype) {
		this.permissiontype = permissiontype;
	}

	public String getname() {
		return name;
	}

	public void setname(String name) {
		this.name = name;
	}

	public int getid() {
		return id;
	}

	public void setid(int id) {
		this.id = id;
	}

	public int getDurationInDays() {
		return durationInDays;
	}

	public void setDurationInDays(int durationInDays) {
		this.durationInDays = durationInDays;
	}
	
	public String getInvalidDurationInDays() {
		return invalidDurationInDays;
	}

	public void setInvalidDurationInDays(String invalidDurationInDays) {
		this.invalidDurationInDays = invalidDurationInDays;
	}
}