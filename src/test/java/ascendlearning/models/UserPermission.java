package ascendlearning.models;

public class UserPermission {
	private String permissionType;
	private String permissionId;
	private String name;
	private String expirationAt;

	public String getPermissionType() {
		return permissionType;
	}

	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}

	public String getPermissionID() {
		return permissionId;
	}

	public void setPermissionID(String permissionId) {
		this.permissionId = permissionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getExpirationAt() {
		return expirationAt;
	}

	public void setExpirationAt(String expirationAt) {
		this.expirationAt = expirationAt;
	}
}
