package ascendlearning.models;

public class Permissions {
	
	    private String permissiontype;
	    private int permissionduration;
	    private String name;
	    private String permissionName;
	    
	    public String getPermissionType() {
	        return permissiontype;
	    }

	    public void setPermissionType(String permissiontype) {
	        this.permissiontype = permissiontype;
	    }
	    
	    public int getPermissionDuration() {
	        return permissionduration;
	    }

	    public void setPermissionDuration(int permissionduration) {
	        this.permissionduration = permissionduration;
	    }
	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }
	    
	    public String getPermissionName() {
	        return permissionName;
	    }
	    
	    public void setPermissionName(String permissionName) {
	        this.permissionName = permissionName;
	    }
}
