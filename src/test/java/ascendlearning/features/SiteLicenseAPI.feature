Feature: SiteLicense API 

@positive 
Scenario: Create a site license 
	Given I have a valid site license data 
	When I create a site license 
	Then I get a 201 response 
	And I get the created license id 
	
@positive 
Scenario: Delete site license with valid site license id 
	Given I have a valid site license 
	When I delete the site license 
	Then I get a 200 response 
	And Verify the site license is deleted 
	And Returned id is same as deleted id 
	
@positive 
Scenario: Verify retrieving a valid site license details 
	Given I have a valid site license 
	When I query the license details 
	Then I get a 200 response 
	And I get the license details 
	
@positive 
Scenario: Verify site license update using site license id 
	Given I have a valid site license 
	When I update the license details 
	Then I get a 200 response 
	And I get the updated license id 
	
@negative 
@toberemoved 
Scenario: Verify retrieving an invalid site license 
	Given I have an invalid site license id 
	When I query the license details 
	Then I get a 404 response 
	And I get error message: Site license cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify site license Update with invalid site license id 
	Given I have an invalid site license id 
	When I update the license details 
	Then I get a 404 response 
	And I get error message: Site license cannot be found 
	
	