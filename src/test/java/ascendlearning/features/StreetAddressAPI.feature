Feature: StreetAddress API 

@positive 
Scenario: Verify valid street address 
	Given I have a valid street address details 
	When I verify the street address 
	Then I get a 200 response 
	
@positive 
Scenario: Verify street address with street address and postal code 
	Given I have a valid address with street address and postal code 
	When I verify the street address 
	Then I get a 200 response 
	
@negative 
@toberemoved 
Scenario: Verify street address by providing invalid postal code 
	Given I have a street address with invalid postal code 
	When I verify the street address 
	Then I get a 422 response 
	And I get message: Address is not deliverable 
	
@negative 
@toberemoved 
Scenario: Verify street address by providing invalid country code 
	Given I have a street address with invalid country code 
	When I verify the street address 
	Then I get a 400 response 
	And I get message: Street Address: Country code is the wrong length 
	
@negative 
@toberemoved 
Scenario: Verify street address without providing country code 
	Given I have a street address without country code 
	When I verify the street address 
	Then I get a 400 response 
	And I get message: Street Address: Country code can't be blank 
	
@negative 
@toberemoved 
Scenario: Verify street address without providing street address1 
	Given I have street address details without street address1 
	When I verify the street address 
	Then I get a 400 response 
	And I get message: Street address can't be blank