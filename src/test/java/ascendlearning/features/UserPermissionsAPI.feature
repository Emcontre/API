Feature: User Permission API 

@positive 
Scenario: Verify updating permission with invalid parameter 
	Given I have an existing user and a valid permission 
	When I update permission with invalid parameters to the user 
	Then I get a 400 response 
	And I get message: Invalid parameters 
	
@negative 
Scenario: Verify deleting permission with invalid permission id 
	Given I have an existing user and a valid permission 
	When I remove the permission for the user using id 
	Then I get a 404 response 
	And I get message: User userId does not have the permission 
	
@negative 
Scenario: Verify assigning a permission with invalid duration to a user 
	Given I have an existing user and a valid permission 
	When I assign the permission with invalid duration to the user 
	Then I get a 400 response 
	And I get message: Data provided could not be parsed as a date 
	
@positive 
Scenario: Verify removing permission from user with user id 
	Given I have an existing user with a permission assigned 
	When I remove the permission from the user using permission id 
	Then I get a 200 response 
	And I receive the same permission id in the delete response 
	
@negative 
Scenario: Verify deleting permission with invalid user id 
	Given I have an invalid user id and a valid permission id 
	When I remove the permission for the user using id 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@positive 
Scenario: Verify updating assigned User permission 
	Given I have an existing user with a permission assigned 
	When I update permission details for the assigned user 
	Then I get a 200 response 
	And I receive the same permission id after update request 
	
@negative 
@toberemoved 
Scenario: Verify retrieving list of permissions with invalid user id 
	Given I have an invalid user 
	When I send a request to retrieve permissions for a user 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@positive 
Scenario: Verify retrieving assigned user permissions 
	Given I have an existing user with a permission assigned 
	When I query the permission assigned to the user 
	Then I get a 200 response 
	And I get the same permission details which was assigned 
	
@positive 
Scenario: Verify assigning a valid permission to a user 
	Given I have an existing user and a valid permission 
	When I assign the permission to the user 
	Then I get a 201 response 
	And I get the updated user id and assigned permission id 
	
@negative 
@toberemoved 
Scenario: Verify assigning a permission to a user with invalid permission id 
	Given I have an existing user 
	When I send a request to assign an invalid permission to the user 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@negative 
@toberemoved
Scenario: Verify assigning a permission to an invalid user 
	Given I have an invalid user 
	When I send a request to assign permission to the user 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify assigning the expiry date to a user permission with invalid data 
	Given I have an existing user and a valid permission 
	When I send a request with expiry date to user permission with invalid data 
	Then I get a 400 response 
	And I get message: Cannot set expiration with provided data 
	
@negative 
@toberemoved 
Scenario: Verify assigning a permission to a user with invalid parameter 
	Given I have an existing user 
	When I send a request with invalid parameter to update the permission 
	Then I get a 400 response 
	And I get error message: Permissions must be an array 
	
@negative 
@toberemoved 
Scenario: To verify assigning a permission to a user with invalid parameter 
	Given I have an invalid user id and a valid permission id 
	When I send a request with invalid user id to update user permission 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify deleting a permission which is already assigned to a user 
	Given I have an existing user with a permission assigned 
	When I send a request to delete the user permission 
	Then I get a 409 response 
	And I get message: Cannot delete permission of the user because it is already assigned 
	
@negative 
@toberemoved 
Scenario: Verify updating an expiration to a permission with invalid data 
	Given I have an existing user with a permission assigned 
	When I send a request to update expiration for the permission with invalid data 
	Then I get a 400 response 
	And I get message: Cannot set expiration with provided data 
	
	