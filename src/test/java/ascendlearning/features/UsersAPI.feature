Feature: Users API 

@positive 
Scenario: Create a new user 
	Given I have valid user details 
	When I create the user 
	Then I get a 201 response 
	
@positive 
Scenario: Verify UA Information through UAID 
	Given I have an existing user 
	When I query the user details by UAID 
	Then I get a 200 response 
	And I get expected user detail 
	
@positive 
Scenario: Verify UA information through email id 
	Given I have an existing user 
	When I query the user details by email id 
	Then I get a 404 response 
	And I get a message: User with id email cannot be found 
	
@positive 
Scenario: Verify deleting a specified user 
	Given I have an existing user 
	When I send a request to delete the user 
	Then I get a 200 response 
	And I receive the same deleted user id 
	
@positive 
Scenario: Verify updating a specified users first name and last name 
	Given I have an existing user 
	When I update first name and last name 
	Then I get a 200 response 
	And I receive the same user id after update 
	And I verify the updated user details 
	
@toberemoved 
@negative 
Scenario: Verify deleting an invalid user 
	Given I have an invalid user id 
	When I send a request to delete the user 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@toberemoved 
@negative 
Scenario: Verify create new user with existing user name 
	Given I have an existing user 
	When I create a new user with existing user name 
	Then I get a 409 response 
	And I get a message: Username is already in use. Try a different username 
	
@positive
Scenario: Verify suspending a user 
	Given I have an existing user 
	When I update the user from active to suspended 
	Then I get a 200 response 
	And I receive the user id in the response for suspend request 
	And I verify the user status as suspended 
	
@positive 
Scenario: Verify reinstating a suspended user 
	Given I have an existing suspended user 
	When I update the user from suspended to active 
	Then I get a 200 response 
	And I receive the user id in the response for reinstate request 
	
@negative 
@toberemoved 
Scenario: Verify suspending an invalid user 
	Given I have an invalid user id 
	When I update the user from active to suspended 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify reinstate an invalid suspended user 
	Given I have an invalid user id 
	When I update the user from suspended to active 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@toberemoved 
@negative 
Scenario: Verify creating new user with existing email id 
	Given I have an existing user 
	When I create a new user with existing email id 
	Then I get a 409 response 
	And I get a message: Email has already been taken 
	
@toberemoved 
@negative 
Scenario: Verify UA Information through invalid UAID 
	Given I have an invalid user id 
	When I query the user details by UAID 
	Then I get a 404 response 
	And I get a message: User with id cannot be found 
	
@positive 
Scenario: Verify updating a specified user 
	Given I have an existing user 
	When I send a request to update user details 
	Then I get a 200 response 
	And I verify the actual user id with expected user id 
	And Verify updated user data 
	
@positive 
Scenario: Verify user password change 
	Given I have an existing user 
	When I send request to change password 
	Then I get a 200 response 
	And Verify the expected user id with actual user id 
	
@negative 
@toberemoved 
Scenario: Verify invalid password change 
	Given I have an existing user 
	When I send request to change password with invalid password 
	Then I get a 400 response 
	And I get message: Password invalid 
	
@negative 
@toberemoved 
Scenario: Verify Suspending a user with invalid suspension reason 
	Given I have an existing user 
	When I send request to suspend the user with invalid suspension reason 
	Then I get a 400 response 
	And I get message: A suspension reason is required