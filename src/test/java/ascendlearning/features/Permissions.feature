Feature: Permissions API 

@positive 
Scenario: Create New Permission 
	Given I have a new permission details 
	When I send a request to create permission 
	Then I get a 201 response 
	
@negative 
@toberemoved 
Scenario: Verify retrieving permissions with invalid permission id 
	Given I have an invalid permission id 
	When I retrieve the permission with invalid permission id 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@negative 
@toberemoved 
Scenario: Verify UA Permission Update With Invalid Permission ID 
	Given I have an invalid permission details 
	When I update permission with invalid permissionId 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@negative 
@toberemoved 
Scenario: Verify deleting a permission with invalid permission id 
	Given I have an invalid permission id 
	When I send a request to delete permission 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@positive 
Scenario: Verify deleting a permission with a valid permission id 
	Given I have an existing permission 
	When I send a request to delete permission 
	Then I get a 200 response 
	And Verify the expected permission id with actual permission id 
	
@positive 
Scenario: Verify permission update using permission id 
	Given I have an existing permission 
	When I update permission with a valid permission id 
	Then I get a 200 response 
	And I receive the same permission id after update request 
	
@positive 
Scenario: Verify permissions retrieved through permission id 
	Given I have an existing permission 
	When I query the permission with valid permission id 
	Then I get a 200 response 
	And Verify the expected permission data with actual permission data