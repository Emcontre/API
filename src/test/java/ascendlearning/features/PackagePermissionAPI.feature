Feature: Package Permission API 

@positive 
Scenario: Verify assigning permissions to a package 
	Given I have an existing permission id and package id 
	When I assign permission to the package 
	Then I get a 201 response 
	And Verify the existing permission id with returned permission id 
	And Verify the existing  package id with returned package id 
	
@positive 
Scenario: Verify updating a permission for a package permission using id 
	Given I have a valid package id with a permission assigned 
	When I update the duration for a specific permission with id 
	Then I get a 200 response 
	And I receive the same permission id after update duration request 
	
@negative 
@toberemoved 
Scenario: Verify updating package permission with invalid permission id 
	Given I have an existing package id and invalid permission id 
	When I update permission to the package by using invalid permission id 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@positive 
Scenario: Verify removing permission from package with permission id 
	Given I have a valid package id with a permission assigned 
	When I delete permission from package with permission id 
	Then I get a 200 response 
	And I get the deleted permission id 
	
@negative 
@toberemoved 
Scenario: Verify updating package permission with valid integer parameter 
	Given I have a valid package id with a permission assigned 
	When I update the permission with invalid integer value as duration 
	Then I get a 400 response 
	And I get message: Duration in days must be an integer 
	
@negative 
@toberemoved 
Scenario: Verify assigning a permission to a package which is already associated with the package 
	Given I have a valid package id with a permission assigned 
	When I assign permission to the same package 
	Then I get a 400 response 
	And I get message: Permission is already assigned to the package 
	
@negative 
Scenario: Verify assigning permission to a package with invalid integer 
	Given I have an existing permission id and package id 
	When I assign the permission to the package with invalid integer 
	Then I get a 400 response 
	And I get message: Duration in days must be an integer 
	
@negative 
@toberemoved 
Scenario: Verify assigning incorrect permission id to package 
	Given I have an existing package id and invalid permission id 
	When I assign the permission to the package with incorrect permission id 
	Then I get a 404 response 
	And I get message: The permission with id does not exist 
	
@negative 
@toberemoved 
Scenario: Verify assign permission to incorrect package id 
	Given I have an existing permission id and incorrect package id 
	When I assign the permission to the package with incorrect package id 
	Then I get a 404 response 
	And I get a message: Package cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify updating duration for a package permission with invalid package id 
	Given I have an existing permission id and incorrect package id 
	When I update the permission duration with incorrect package id 
	Then I get a 404 response 
	And I get a message: Package id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify deleting permission with invalid package id 
	Given I have an existing permission id and incorrect package id 
	When I delete permission for the invalid package 
	Then I get a 404 response 
	And I get a message: Package id cannot be found 
	
@positive 
Scenario: Verify deleting permission with invalid permission id 
	Given I have an existing package id and invalid permission id 
	When I delete permission for the valid package with invalid permission id 
	Then I get a 404 response 
	And I get message: The permission with id does not exist