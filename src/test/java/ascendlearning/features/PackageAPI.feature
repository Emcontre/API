Feature: Package API 

@positive 
Scenario: Create a package 
	Given I have valid package details 
	When I create a package 
	Then I get a 201 response 
	
@positive 
Scenario: Retrieve all package 
	When I send a request to get all packages 
	Then I get a 200 response 
	
@positive 
Scenario: Verify updating packages with package id 
	Given I have a valid package 
	When  I send a request for updating package through package id 
	Then  I get a 200 response 
	And  I receive the same package id after update request 
	
@positive 
Scenario: Verify deleting package through package id 
	Given I have a valid package 
	When I send a request for deleting package through package id 
	Then I get a 200 response 
	And I receive the same package id after delete request 
	
@negative 
@toberemoved 
Scenario: Verify retrieving package with invalid package id 
	Given I have an invalid package id 
	When I retrieve the package with invalid package id 
	Then I get a 404 response 
	And I get a message: Package id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify updating package with invalid package id 
	Given I have an invalid package id 
	When I update the package with invalid package id 
	Then I get a 404 response 
	And I get a message: Package id cannot be found 
	
@negative 
@toberemoved 
Scenario: Verify creating a packages with blank parameter 
	Given I have package details with blank parameters 
	When I create a package 
	Then I get a 400 response 
	And I get message: Package name cannot be blank/Package already exists 
	
@negative 
@toberemoved 
Scenario: Verify Creating a Packages with same name 
	Given I have a valid package 
	When I create a package with the same name 
	Then I get a 400 response 
	And I get message: Package name cannot be blank/Package already exists 
