package ascendlearning.steps;

import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.SystemEnvironmentVariables;

class CustomScenarioSteps {

    /**
     * Retrieves the default URL from the "serenity.properties" file for the "api.base.url" property
     *
     * @return the default API URL set in "serenity.properties"
     */
    String defaultUrl() {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return variables.getProperty("api.base.url");
    }
    String permissionUrl() {
        EnvironmentVariables variables = SystemEnvironmentVariables.createEnvironmentVariables();
        return variables.getProperty("permission.base.url");
    }
}
