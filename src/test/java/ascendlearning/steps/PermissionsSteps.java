package ascendlearning.steps;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ascendlearning.data.CommonData;
import ascendlearning.data.PermissionsData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

public class PermissionsSteps extends CustomScenarioSteps {

	@Given("^I have a new permission details$")
	public void aValidNewPermission() {
		JSONObject permission = PermissionsData.validPermissionInformation();
		Serenity.setSessionVariable("permission").to(permission);
	}
	
	@Given("^I have an invalid permission id$")
    public void aPermissionWithInvalidID() {
        int permissionId = CommonData.PERMISSIONID;
        Serenity.setSessionVariable("permissionId").to(permissionId);
    }
	
	@Given("^I have an invalid permission details$")
	public void anInvalidPermissionDetails() {
		HashMap<String, String> permission = PermissionsData.updatePermissionDetails();
		int permissionId = CommonData.PERMISSIONID;
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("permission").to(permission);
	}

	@Given("^I have an existing permission$")
	public void iHaveAnExistingPermission() {
		JSONObject permission = PermissionsData.validPermissionInformation();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permission)
				.when()
					.post(defaultUrl() + "/permissions");
		int permissionId = response.body().jsonPath().get("data.permissions[0]");
		JSONArray permissions = (JSONArray) permission.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();
        Serenity.setSessionVariable("permissionName").to(permissionName);
        Serenity.setSessionVariable("permissionType").to(permissionType);
		Serenity.setSessionVariable("permissionId").to(permissionId);
	}

	@When("^I send a request to create permission$")
	public void iCreateASiteLicense() {
		JSONObject postBody = Serenity.sessionVariableCalled("permission");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/permissions");
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I retrieve the permission with invalid permission id$")
    public void iRetrieveThePermissionWithInvalidPermissionId() {
        int permissionID = Serenity.sessionVariableCalled("permissionId");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .get(defaultUrl() + "/permissions/"+permissionID);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(permissionID);
    }
	
	@When("^I update permission with invalid permissionId$")
    public void iUpdatePermissionWithInvalidPermissionId() {
		HashMap<String, String> patchBody = Serenity.sessionVariableCalled("permission");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(patchBody)
                .when()
                    .patch(defaultUrl() + "/permissions/" + permissionId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(permissionId);
    }
	
	@When("^I send a request to delete permission$")
	public void iSendARequestToDeletePermission() 
	{
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/permissions/"+permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);
	}
	
	@When("^I update permission with a valid permission id$")
    public void iUpdatePermissionWithAValidPermissionId() {
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> patchBody = PermissionsData.permissionDetails(); //permissionDetails
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(patchBody)
                .when()
                    .patch(defaultUrl() + "/permissions/" + permissionId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(permissionId);
    }
	
	@When("^I send a request to update the permission$")
	public void iSendARequestToUpdateThePermission() 
	{		
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> update = PermissionsData.updatePermissionDetailsByPermissionId();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.body(update)
					.patch(defaultUrl() + "/permissions/"+permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);
	}
	
	@When("^I query the permission with valid permission id$")
	public void iQueryThePermissionWithValidPermissionId() {
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		String permissionType = Serenity.sessionVariableCalled("permissionType");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("expPermissionName").to(permissionName);
		Serenity.setSessionVariable("expPermissionType").to(permissionType);
	}
}
