package ascendlearning.steps;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ascendlearning.data.CommonData;
import ascendlearning.data.PackageData;
import ascendlearning.data.PackagePermissionData;
import ascendlearning.data.PermissionsData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import io.restassured.response.Response;

public class PackagePermissionAPISteps extends CustomScenarioSteps {

	@Given("^I have an existing permission id and package id$")
	public void iHaveAnExistingPermissionIdAndPackageId() {
		JSONObject permission = PermissionsData.validPermissionInformation();
		Response responsePermission = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permission)
				.when()
					.post(defaultUrl() + "/permissions");
		int permissionId = responsePermission.body().jsonPath().get("data.permissions[0]");
		HashMap<String, String> postPackageBody = PackageData.randomPackage();
		Response responsePackage = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postPackageBody)
				.when()
					.post(defaultUrl() + "/packages");
		int packageId = responsePackage.body().jsonPath().get("data.package_id");
		JSONArray permissions = (JSONArray) permission.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();
        Serenity.setSessionVariable("permissionName").to(permissionName);
        Serenity.setSessionVariable("permissionType").to(permissionType);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("packageId").to(packageId);
	}
	
	@Given("^I have a valid package id with a permission assigned$")
	public void iHaveAValidPackageIdAndPermissionId() {
		HashMap<String, String> postBody = PackageData.randomPackage();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/packages");
		int packageId = response.body().jsonPath().get("data.package_id");
		JSONObject permission = PermissionsData.validPermissionInformation();
		Response permissionResponse = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permission)
				.when()
					.post(defaultUrl() + "/permissions");
		int permissionId = permissionResponse.body().jsonPath().get("data.permissions[0]");
		JSONArray permissions = (JSONArray) permission.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();
		JSONObject post = PackagePermissionData.patchPermissionToPackage(permissionId,permissionName,permissionType);
		Response assignedPermission = SerenityRest.given()
				.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
				.header("Content-Type", "application/json").body(post).when()
				.post(defaultUrl() + "/packages/" + packageId + "/permissions");
		
		Serenity.setSessionVariable("assignedPermission").to(assignedPermission);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("permissionName").to(permissionName);
		Serenity.setSessionVariable("permissionType").to(permissionType);
		Serenity.setSessionVariable("packageId").to(packageId);
	}
	
	@Given("^I have an existing package id and invalid permission id$")
	public void iHaveAnExistingPackageIdAndInvalidPermissionId() {
		HashMap<String, String> packagePostBody = PackageData.randomPackage();
		Response responsePackage = SerenityRest
			.given()
				.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
				.header("Content-Type", "application/json")
				.body(packagePostBody)
			.when()
				.post(defaultUrl() + "/packages");
		int packageId = responsePackage.body().jsonPath().get("data.package_id");
		int invalidPermissionID = CommonData.INVALIDPERMISSIONID;
		Serenity.setSessionVariable("invalidPermissionID").to(invalidPermissionID);
		Serenity.setSessionVariable("packageId").to(packageId);
		Serenity.setSessionVariable("samplePermissionName").to(CommonData.SAMPLEPERMISSIONNAME);
		Serenity.setSessionVariable("samplePermissionType").to(CommonData.SAMPLEPERMISSIONTYPE);
	}
	
	@Given("^I have an existing permission id and incorrect package id$")
    public void iHaveAnExistingPermissionIdAndIncorrectPackageId() {
		int packageId =CommonData.INVALIDPACKAGEID;
		JSONObject permission = PermissionsData.validPermissionInformation();
        Response responsePermission = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(permission)
                .when()
                    .post(defaultUrl() + "/permissions");
        int permissionId = responsePermission.body().jsonPath().get("data.permissions[0]");
        JSONArray permissions = (JSONArray) permission.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();
        Serenity.setSessionVariable("permissionName").to(permissionName);
        Serenity.setSessionVariable("permissionType").to(permissionType);
        Serenity.setSessionVariable("permissionId").to(permissionId);
        Serenity.setSessionVariable("packageId").to(packageId);
    }

	@When("^I assign permission to the package$")
	public void iAssignPermissionToThePackage() {
		int packageId = Serenity.sessionVariableCalled("packageId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		String permissionType = Serenity.sessionVariableCalled("permissionType");
		JSONObject postBody = PackagePermissionData.patchPermissionToPackage(permissionId, permissionName,
				permissionType);
		Response response = SerenityRest.given()
				.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
				.header("Content-Type", "application/json").body(postBody).when()
				.post(defaultUrl() + "/packages/" + packageId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("packageId").to(packageId);		
	}

	@When("^I update the duration for a specific permission with id$")
	public void iUpdateTheDurationForASpecificPermissionWithId() {
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		int packageId = Serenity.sessionVariableCalled("packageId");
		HashMap<String, Object> update = PackagePermissionData.updatePermission();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(update)
				.when()
					.patch(defaultUrl() + "/packages/" + packageId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);
	}
	
	@When("^I update permission to the package by using invalid permission id$")
	public void iUpdatePermissionToPackageByUsingInvalidPermissionId() {
		int packageId = Serenity.sessionVariableCalled("packageId");
		int invalidPermissionID = Serenity.sessionVariableCalled("invalidPermissionID");
		HashMap<String, Object> postBody = PackagePermissionData.updatePermission();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.patch(defaultUrl() + "/packages/" + packageId + "/permissions/" + invalidPermissionID);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(invalidPermissionID);
	}

	@When("^I delete permission from package with permission id$")
	public void iDeletePermissionFromPackageWithPermissionId() {
	 int packageId = Serenity.sessionVariableCalled("packageId");
	 int permissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/packages/"+packageId+"/permissions/"+permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);
	}

	@When("^I update the permission with invalid integer value as duration$")
	public void iUpdateThePermissionWithInvalidIntegerValueAsDuration() {
		int packageId = Serenity.sessionVariableCalled("packageId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> patchBody = PackagePermissionData.permissionWithInvalidIntegerParameter();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/packages/" + packageId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I assign permission to the same package$")
	public void iTryToAlreadyAssignedPermissionToAnExistingPackage() {
	 int packageId = Serenity.sessionVariableCalled("packageId");
	 int permissionId = Serenity.sessionVariableCalled("permissionId");
	 String permissionName = Serenity.sessionVariableCalled("permissionName");
	 String permissiontype = Serenity.sessionVariableCalled("permissiontype");
	 JSONObject assignPermissionBody = PackagePermissionData.patchPermissionToPackage(permissionId, permissionName, permissiontype);
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(assignPermissionBody)
				.when()
					.post(defaultUrl() + "/packages/"+packageId+"/permissions");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("packageId").to(packageId);
		Serenity.setSessionVariable("permissionName").to(permissionName);
	}
	
	@When("^I assign the permission to the package with invalid integer$")
    public void iAssignThePermissionToThePackageWithInvalidInteger() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int permissionId = Serenity.sessionVariableCalled("permissionId");
        String permissionName = Serenity.sessionVariableCalled("permissionName");
        String permissiontype = Serenity.sessionVariableCalled("permissiontype");
        JSONObject postBody = PackagePermissionData.assignPermissionToPackageWithInvalidInteger(permissionId, permissionName,
                permissiontype);
        Response response = SerenityRest
            .given()
                .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                .header("Content-Type", "application/json")
                .body(postBody)
            .when()
                .post(defaultUrl() + "/packages/" + packageId + "/permissions");
        Serenity.setSessionVariable("response").to(response);
    }
	
	@When("^I assign the permission to the package with incorrect permission id$")
    public void iAssignPermissionToThePackageWithIncorrectPermissionId() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int invalidPermissionId = Serenity.sessionVariableCalled("invalidPermissionID");
        String samplePermissionName = Serenity.sessionVariableCalled("samplePermissionName");
        String samplePermissionType = Serenity.sessionVariableCalled("samplePermissionType");
        JSONObject postBody = PackagePermissionData.patchPermissionToPackage(invalidPermissionId,samplePermissionName,samplePermissionType);
        Response response = SerenityRest
            .given()
                .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                .header("Content-Type", "application/json").
                 body(postBody).
            when()
                .post(defaultUrl() + "/packages/" + packageId + "/permissions");
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(invalidPermissionId);
    }
	
	@When("^I assign the permission to the package with incorrect package id$")
    public void iAssignPermissionToThePackageWithIncorrectPackageId() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int permissionId = Serenity.sessionVariableCalled("permissionId");
        String permissionName = Serenity.sessionVariableCalled("permissionName");
        String permissionType = Serenity.sessionVariableCalled("permissionType");
        JSONObject postBody = PackagePermissionData.patchPermissionToPackage(permissionId, permissionName,
                permissionType);
        Response response = SerenityRest
        		.given()
        			.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
        			.header("Content-Type", "application/json")
        			.body(postBody)
                .when()
                	.post(defaultUrl() + "/packages/" + packageId + "/permissions");
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(permissionId);
        Serenity.setSessionVariable("packageId").to(packageId);       
    }
	
	@When("^I update the permission duration with incorrect package id$")
    public void iUpdateThePermissionDurationWithIncorrectPackageId() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int permissionId = Serenity.sessionVariableCalled("permissionId");
        HashMap<String, Object> patchBody = PackagePermissionData.updatePermission();
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(patchBody)
                .when()
                    .patch(defaultUrl() + "/packages/" + packageId + "/permissions/" + permissionId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("packageId").to(packageId);
    }
	
	@When("^I delete permission for the invalid package$")
    public void iDeletePermissionForTheInvalidPackage() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int permissionId = Serenity.sessionVariableCalled("permissionId");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
               
                .when()
                    .delete(defaultUrl() + "/packages/" + packageId + "/permissions/" + permissionId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("packageId").to(packageId);
    }
	
	@When("^I delete permission for the valid package with invalid permission id$")
    public void iDeletePermissionForTheValidPackageWithInvalidPermissionId() {
        int packageId = Serenity.sessionVariableCalled("packageId");
        int permissionId = Serenity.sessionVariableCalled("invalidPermissionID");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .delete(defaultUrl() + "/packages/" + packageId + "/permissions/" + permissionId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("permissionId").to(permissionId);
    }
}
