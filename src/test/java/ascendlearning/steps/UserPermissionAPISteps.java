package ascendlearning.steps;

import java.util.HashMap;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ascendlearning.data.CommonData;
import ascendlearning.data.PermissionsData;
import ascendlearning.data.UserPermissionData;
import ascendlearning.data.UsersData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

public class UserPermissionAPISteps extends CustomScenarioSteps{
    
	@Given("^I have an existing user and a valid permission$")
	public void iHaveAnExistingUserAndAValidPermission() {
		HashMap<String, String> userPostBody = UsersData.randomActiveUser();
		Response userResponse = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(userPostBody)
				.when()
					.post(defaultUrl() + "/users");
		JSONObject permissionPostBody = PermissionsData.validPermissionInformation();
		Response responsePermission = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permissionPostBody)
				.when()
					.post(defaultUrl() + "/permissions");
		int userId = userResponse.body().jsonPath().get("data.user_id");
		int permissionId = responsePermission.body().jsonPath().get("data.permissions[0]");
		JSONArray permissions = (JSONArray) permissionPostBody.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();
        Serenity.setSessionVariable("permissionType").to(permissionType);
        Serenity.setSessionVariable("permissionName").to(permissionName);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("userId").to(userId);
	}
	
	@Given("^I have an existing user with a permission assigned$")
	public void iHaveAnExistingUserWithAPermissionAssigned() {
		HashMap<String, String> userPostBody = UsersData.randomActiveUser();
		Response userResponse = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(userPostBody)
				.when()
					.post(defaultUrl() + "/users");
		JSONObject permissionPostBody = PermissionsData.validPermissionInformation();
		Response responsePermission = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permissionPostBody)
				.when()
					.post(defaultUrl() + "/permissions");
		int userId = userResponse.body().jsonPath().get("data.user_id");
		int permissionId = responsePermission.body().jsonPath().get("data.permissions[0]");
		HashMap<String, String> patchBody = UserPermissionData.patchPermissionToUser();
		Response patchResponse = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		
		JSONArray permissions = (JSONArray) permissionPostBody.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        String permissionName = permissionDetails.get("name").toString();
        String permissionType = permissionDetails.get("permission_type").toString();        
        Serenity.setSessionVariable("permissionName").to(permissionName);
        Serenity.setSessionVariable("permissionType").to(permissionType);
		Serenity.setSessionVariable("patchResponse").to(patchResponse);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("userId").to(userId);
	}
	
	@Given("^I have an invalid user id and a valid permission id$")
	public void iHaveAnInvalidUserIdAndValidPermissionId() {
		int invalidUserId = CommonData.INVALIDUSERID;
		JSONObject postBody = PermissionsData.validPermissionInformation();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/permissions");
		int permissionId = response.body().jsonPath().get("data.permissions[0]");
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("userId").to(invalidUserId);
	}
	
	@Given("^I have an invalid user$")
	public void iHaveAnInvalidUser() {
		int invalidUserId = CommonData.INVALIDUSERID;
		Serenity.setSessionVariable("userId").to(invalidUserId);
	}
	
	@When("^I update permission with invalid parameters to the user$")
	public void iUpdatePermissionWithInvalidParametersToTheUser() {
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		int userId = Serenity.sessionVariableCalled("userId");
		JSONObject permission = UserPermissionData.invalidPermissionDetails();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permission)
				.when()
					.patch(defaultUrl()+"/users/"+ userId +"/permissions/"+permissionId);
		Serenity.setSessionVariable("response").to(response);
	}

	@When("^I remove the permission for the user using id")
	public void iRemovePermissionFromUserUsingID() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("userUAId").to(userId);
		Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionName").to(permissionName);
	}
	
	@When("^I remove the permission from the user using permission id$")
	public void iRemoveThePermissionFromTheUserusingUserId() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userId").to(userId);
	}	
	
	@When("^I assign the permission with invalid duration to the user$")
	public void iAssignThePermissionWithInvalidDurationToTheUser() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		JSONObject postBody = UserPermissionData.updatePermissionwithInvalidDuration(permissionId);
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I update permission details for the assigned user$")
	public void iUpdatePermissionDetailsForTheAssignedUser() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> patchBody = UserPermissionData.patchPermissionToUser();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("permissionId").to(permissionId);		
	}
	
	@When("^I send a request to retrieve permissions for a user$")
	public void iSendARequestToRetrievePermissionsForAUser() {
		int userId = Serenity.sessionVariableCalled("userId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userUAId").to(userId);
	}
	
	@When("^I query the permission assigned to the user$")
	public void iQueryThePermissionAssignedToTheUser() {
		int userId = Serenity.sessionVariableCalled("userId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		String permissionType = Serenity.sessionVariableCalled("permissionType");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .get(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("expectedPermissionName").to(permissionName);
		Serenity.setSessionVariable("expectedPermissionType").to(permissionType);
		Serenity.setSessionVariable("expectedPermissionId").to(permissionId);
	}
	
	@When("^I assign the permission to the user$")
    public void iAssignThePermissionToTheUser() {
        int userId = Serenity.sessionVariableCalled("userId");
        int permissionId = Serenity.sessionVariableCalled("permissionId");
        String permissionType = Serenity.sessionVariableCalled("permissionType");
        String permissionName = Serenity.sessionVariableCalled("permissionName");
        
        
        JSONObject postBody = UserPermissionData.assignPermissionToUser(permissionId,permissionName,permissionType);
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/users/" + userId + "/permissions");
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("userId").to(userId);
        Serenity.setSessionVariable("expectedPermissionId").to(permissionId);
    }
	
	@When("^I send a request to assign an invalid permission to the user$")
	public void iSendARequestToAssignAnInvalidPermissionToTheUser() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		JSONObject postBody = UserPermissionData.invalidPermissionId();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users/" + userId + "/permissions");
		JSONArray permissions = (JSONArray) postBody.get("permissions");
        JSONObject permissionDetails = (JSONObject) permissions.get(0);
        int permissionId = (int) permissionDetails.get("id");
        Serenity.setSessionVariable("permissionId").to(permissionId);
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I send a request to assign permission to the user$")
	public void iSendARequestToAssignPermissionToTheUser() {
		int userId = Serenity.sessionVariableCalled("userId");
		JSONObject permissionDataWithInvalidID = PermissionsData.validPermissionInformation();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permissionDataWithInvalidID)
				.when()
					.post(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userUAId").to(userId);
	}

	@When("^I send a request with expiry date to user permission with invalid data$")
	public void iSendPostRequestWithExpiryDateToPermissionWithInvalidData() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		String permissionType = Serenity.sessionVariableCalled("permissionType");
		JSONObject permissionDataWithExpiryDate = UserPermissionData.validPermissionInformationWithInvalidExpiryDate(permissionId,permissionName,permissionType);
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(permissionDataWithExpiryDate)
				.when()
					.post(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I send a request with invalid parameter to update the permission$")
	public void iSendARequestWithInvalidParameterToUpdateThePermission() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		JSONObject postBody = UserPermissionData.invalidPermissionArray();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users/" + userId + "/permissions");
		Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I send a request with invalid user id to update user permission$")
	public void iSendPatchRequestWithInvaliduserIdToUpdateUserPermission() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> patchBody = PermissionsData.updateInvalidPermission();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userUAId").to(userId);
	}
	 
	@When("^I send a request to delete the user permission$")
	public void iSendARequestToDeleteTheUserPermission() {
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/permissions/" + permissionId);
		Serenity.setSessionVariable("permissionName").to(permissionName);
	    Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I send a request to update expiration for the permission with invalid data$")
	public void iSendARequestToUpdateExpirationForThePermissionWithInvalidData() {
		int userId = Serenity.sessionVariableCalled("userId");
		int permissionId = Serenity.sessionVariableCalled("permissionId");
		HashMap<String, String> patchBody = UserPermissionData.invalidExpiryDataForPermissionUpdate();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/users/" + userId + "/permissions/" + permissionId);
		Serenity.setSessionVariable("response").to(response);
	}
}