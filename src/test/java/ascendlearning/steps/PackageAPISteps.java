package ascendlearning.steps;

import ascendlearning.data.CommonData;
import ascendlearning.data.PackageData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import java.util.HashMap;

public class PackageAPISteps extends CustomScenarioSteps {

    @Given("^I have valid package details$")
    public void iHaveValidPackageDetails() {
    	HashMap<String, String> pack = PackageData.randomPackage();
        Serenity.setSessionVariable("package").to(pack);
    }
    
    @Given("^I have a valid package$")
    public void iHaveAValidPackage() {
    	HashMap<String, String> postBody = PackageData.randomPackage();
    	Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/packages");
    	int packageId = response.body().jsonPath().get("data.package_id");
    	String packageName = postBody.get("name");
        Serenity.setSessionVariable("packageId").to(packageId);
        Serenity.setSessionVariable("packageName").to(packageName);
    }
    
    @Given("^I have an invalid package id$")
    public void iHaveAnInvalidPackageid() {
        int packageId=CommonData.INVALIDPACKAGEID;
        Serenity.setSessionVariable("packageId").to(packageId);
    }
    
    @Given("^I have package details with blank parameters$")
	public void iHaveBlankParameter() 
	{
		HashMap<String, String> pack = PackageData.packageDetailsWithBlankParameters();
		Serenity.setSessionVariable("package").to(pack);
	}
    
    @When("^I create a package$")
    public void iCreateAPackage() {
    	HashMap<String, String> postBody = Serenity.sessionVariableCalled("package");
    	Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/packages");
        Serenity.setSessionVariable("response").to(response);
    }
    
    @When("^I create a package with the same name$")
    public void iCreateAPackageWithTheSameName() {
       	String packageName = Serenity.sessionVariableCalled("packageName");
    	HashMap<String, String> postBody = PackageData.packageWithSameName(packageName);
       	Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/packages");
        Serenity.setSessionVariable("response").to(response);
    }
    
    @When("^I send a request to get all packages$")
    public void iSendARequestToGetAllPackages() {
    	Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .get(defaultUrl() + "/packages");
        Serenity.setSessionVariable("response").to(response);
    }
    
    @When("^I send a request for updating package through package id$")
	public void iSendARequestForUpdatingPackageThroughPackageId()
	{
		int packageId = Serenity.sessionVariableCalled("packageId");
		HashMap<String, String> updateBody = PackageData.randomPackage();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(updateBody)
				.when()
					.patch(defaultUrl() +"/packages/"+ packageId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("packageId").to(packageId);
	}
    
    @When("^I send a request for deleting package through package id$")
	public void iSendARequestForDeletingPackageThroughPackageId()
	{
		int packageId = Serenity.sessionVariableCalled("packageId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() +"/packages/"+ packageId);
		Serenity.setSessionVariable("response").to(response);
	}
    
    @When("^I retrieve the package with invalid package id$")
    public void iRetrieveThePackageWithInvalidPackageId(){
        int packageId = Serenity.sessionVariableCalled("packageId");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .get(defaultUrl() +"/packages/"+ packageId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("packageId").to(packageId);
    }
    
    @When("^I update the package with invalid package id$")
    public void iUpdateThePackageWithInvalidPackageId()
    {
        int packageId = Serenity.sessionVariableCalled("packageId");
        HashMap<String, String> patchBody = PackageData.randomPackage();
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(patchBody)
                .when()
                 .patch(defaultUrl() +"/packages/"+ packageId);
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("packageId").to(packageId);
    }
}
