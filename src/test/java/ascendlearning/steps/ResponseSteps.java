package ascendlearning.steps;

import static org.assertj.core.api.Assertions.assertThat;
import cucumber.api.java.en.Then;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import static org.hamcrest.Matchers.is;

public class ResponseSteps extends CustomScenarioSteps {

	@Then("^I get a success response$")
	public void iGetASuccessResponse() {
		Response response = Serenity.sessionVariableCalled("response");
		response.then().statusCode(is(200));
	}

	@Then("^I get a (\\d+) response$")
	public void iGetAXResponse(int responseCode) {
		Response response = Serenity.sessionVariableCalled("response");
		response.then().statusCode(is(responseCode));
	}

	@Then("^I get expected user detail$")
	public void iGetExpectedUserDetails() {
		Response response = Serenity.sessionVariableCalled("response");
		int userId = Serenity.sessionVariableCalled("userUAId");
		int id = response.body().jsonPath().get("data.id");
		assertThat(id).isEqualTo(userId);
	}
	
	@Then("^I get the created license id$")
	public void iGetTheCreatedLicenseId() {
		Response response = Serenity.sessionVariableCalled("response");
		int siteLicenseId = response.body().jsonPath().get("data.site_license_id");
		assertThat(siteLicenseId>0);
	}
	
	@Then("^Verify the site license is deleted$")
	public void verifyTheSiteLicenseIsDeleted() {
		int createdSitelicenseID = Serenity.sessionVariableCalled("createdSitelicenseID");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/site_licenses" + "/" + createdSitelicenseID);
		response.then().statusCode(is(404));		
	}
	
	@Then("^Returned id is same as deleted id$")
	public void returnedIdIsSameAsDeletedId() {
		int expectedSitelicenseID = Serenity.sessionVariableCalled("createdSitelicenseID");
		Response response = Serenity.sessionVariableCalled("response");
		int deletedSitelicenseID = response.body().jsonPath().get("data.site_license_id");
		assertThat(expectedSitelicenseID).isEqualTo(deletedSitelicenseID);
	}
	
	@Then("^I get the license details$")
	public void iGetTheLicenseDetails() {
		Response response = Serenity.sessionVariableCalled("response");
		int createdSitelicenseID = Serenity.sessionVariableCalled("siteLicenseId");
		int returnedSiteLicenseId = response.body().jsonPath().get("data.id");
		String returnedIdForInstitution = response.body().jsonPath().get("data.id_for_institution");
		assertThat(createdSitelicenseID).isEqualTo(returnedSiteLicenseId);
		assertThat(returnedIdForInstitution).isEqualTo("test1-institution");
	}

	@Then("^I get the updated license id$")
	public void iGetTheUpdatedLicenseDetails() {
		Response response = Serenity.sessionVariableCalled("response");
		int createdSitelicenseID = Serenity.sessionVariableCalled("siteLicenseId");
		int updatedSitelicenseID = response.body().jsonPath().get("data.site_license_id");
		assertThat(createdSitelicenseID).isEqualTo(updatedSitelicenseID);
	}
	
	@Then("^I get message: Address is not deliverable$")
	public void iGetErrorResponseMessageForAddress() {
		Response response = Serenity.sessionVariableCalled("response");
		String returnedMessage = response.body().jsonPath().get("message");
		assertThat(returnedMessage).isEqualTo("Address is not deliverable");
	}	
	
	@Then("^I get a message: User with id email cannot be found$")
	public void iGetErrorResponseSearchingByEmailAddress() {
		Response response = Serenity.sessionVariableCalled("response");
		String createdUserEmailID = Serenity.sessionVariableCalled("createdUserEmailId");
		String returnedMessage = response.body().jsonPath().get("message");
		String expectedMessage = "User with id "+createdUserEmailID+" cannot be found";
		assertThat(returnedMessage).isEqualTo(expectedMessage);
	}
	
	@Then("^I receive the same deleted user id$")
	public void iReceiveTheSameDeletedUserId() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedUserId = Serenity.sessionVariableCalled("userUAId");
		int actualUserId = response.body().jsonPath().get("data.user_id");
		assertThat(expectedUserId).isEqualTo(actualUserId);
	}
	
	@Then("^I receive the same user id after update$")
	public void iReceiveTheSameUserIdAfterUpdate() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedUserId = Serenity.sessionVariableCalled("createdUserId");
		int actualUserId = response.body().jsonPath().get("data.user_id");
		assertThat(expectedUserId).isEqualTo(actualUserId);
	}
	
	@Then("^I verify the updated user details$")
	public void iVerifyTheUpdatedUserDetails() {
		int createdUserId = Serenity.sessionVariableCalled("createdUserId");
		String expectedFirstName = Serenity.sessionVariableCalled("newFirstName");
		String expectedLastName = Serenity.sessionVariableCalled("newLastName");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/users/" + createdUserId);
		String actualFirstName = response.body().jsonPath().get("data.first_name");
		String actualLastName = response.body().jsonPath().get("data.last_name");
		response.then().statusCode(is(200));
		assertThat(actualFirstName).isEqualTo(expectedFirstName);
		assertThat(actualLastName).isEqualTo(expectedLastName);
	}
	
	@Then("^I get error message: Site license cannot be found$")
	public void iGetErrorResponseForInvalidSiteLicenseDeletion() {
		Response response = Serenity.sessionVariableCalled("response");
		int invalidSiteLicenseId = Serenity.sessionVariableCalled("siteLicenseId");
		String actualMessage = response.body().jsonPath().get("message");
		String expectedMessage = "Site license " + invalidSiteLicenseId + " cannot be found";
		assertThat(actualMessage).isEqualTo(expectedMessage);
	}
	
	@Then("^I get message: Street Address: Country code is the wrong length")
    public void iGetErrorResponseMessageForIncorrectCountryCodeLength() {
        Response response = Serenity.sessionVariableCalled("response");
        String returnedMessage = response.body().jsonPath().get("message");
        assertThat(returnedMessage).isEqualTo("Street Address: Country code is the wrong length (should be 2 characters), is invalid");
    }
	
	@Then("^I get message: Street Address: Country code can't be blank")
    public void iGetErrorResponseMessageForBlankCountryCode() {
        Response response = Serenity.sessionVariableCalled("response");
        String returnedMessage = response.body().jsonPath().get("message");
        assertThat(returnedMessage).isEqualTo("Street Address: Country code can't be blank");
    }
	
	@Then("^I get message: The permission with id does not exist")
    public void iGetNextResponseMessageForInvalidPermissionID() {
        Response response = Serenity.sessionVariableCalled("response");
        int permissionID = Serenity.sessionVariableCalled("permissionId");
        String expectedMessage = "The permission with id " + permissionID + " does not exist";
        String actualMessage = response.body().jsonPath().get("message");
        assertThat(expectedMessage).isEqualTo(actualMessage);
    }
	
	@Then("^I get message: Street address can't be blank$")
	public void iGetErrorMessageStreetAddressCannotBeBlank() {
		Response response = Serenity.sessionVariableCalled("response");
		String message = response.body().jsonPath().get("message");
		assertThat(message).isEqualTo("Street Address: Street address1 can't be blank");
	}
	
	@Then("^I get a message: Username is already in use. Try a different username$")
    public void iGetErrorResponseCreatingUserWithExisitingUserName() {
        Response response = Serenity.sessionVariableCalled("response");
        String existingUserName = Serenity.sessionVariableCalled("existingUserName");
        String returnedMessage = response.body().jsonPath().get("message");
        String expectedMessage = "Username "+ existingUserName +" is already in use. Try a different username";
        assertThat(returnedMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I receive the user id in the response for suspend request$")
    public void iReceiveTheUserIdThatSuspended() {
        Response response = Serenity.sessionVariableCalled("response");
        int expectedUserId = Serenity.sessionVariableCalled("userUAId");
        int actualUserId = response.body().jsonPath().get("data.user_id");
        assertThat(expectedUserId).isEqualTo(actualUserId);
    }
   
    @Then("^I verify the user status as suspended$")
    public void iVerifyTheUserStatusAsSuspended() {
        int suspendedUserId = Serenity.sessionVariableCalled("userUAId");
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                .when()
                    .get(defaultUrl() + "/users/" + suspendedUserId);
        response.then().statusCode(is(200));
        Boolean userStatus = response.body().jsonPath().get("data.suspended");
        assertThat(userStatus).isEqualTo(true);
    }
    
    @Then("^I receive the user id in the response for reinstate request$")
    public void iReceiveTheUserIdInTheResponseForReinstateRequest() {
        Response response = Serenity.sessionVariableCalled("response");
        int expectedUserId = Serenity.sessionVariableCalled("userUAId");
        int actualUserId = response.body().jsonPath().get("data.user_id");
        assertThat(expectedUserId).isEqualTo(actualUserId);
    }
     
    @Then("^I get a message: User with id cannot be found$")
	public void iGetErrorResponseForInvalidUserId() {
		Response response = Serenity.sessionVariableCalled("response");
		int userUAID = Serenity.sessionVariableCalled("userUAId");
		String returnedMessage = response.body().jsonPath().get("message");
		String expectedMessage = "User with id " + userUAID +" cannot be found";
		assertThat(returnedMessage).isEqualTo(expectedMessage);
	}
    
    @Then("^I get a message: Email has already been taken$")
    public void iGetErrorForCreatingUserWithExisitingUserEmailID() {
        Response response = Serenity.sessionVariableCalled("response");
        String returnedMessage = response.body().jsonPath().get("message");
        String expectedMessage = "Email has already been taken";
        assertThat(returnedMessage).isEqualTo(expectedMessage);
    }

    @Then("^Verify the expected permission id with actual permission id$")
	public void verifyPermissionId() {
		int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = Serenity.sessionVariableCalled("response");
		int actualPermissionId = response.body().jsonPath().get("data.id");
		assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
	}
    
    @Then("^I get message: Invalid parameters$")
	public void iGetMessageWithInavlidParameters() {
    	  Response response = Serenity.sessionVariableCalled("response");
          String returnedMessage = response.body().jsonPath().get("message");
          String expectedMessage ="Invalid parameters";
          assertThat(returnedMessage).isEqualTo(expectedMessage);
	}

    @Then("^I get message: User userId does not have the permission$")
    public void iGetErrorResponseForUserNotHavingthePermissionAssigned() {
    	int userId = Serenity.sessionVariableCalled("userUAId");
        Response response = Serenity.sessionVariableCalled("response");
        String permissionName = Serenity.sessionVariableCalled("permissionName");
        String actualMessage = response.body().jsonPath().get("message");
        String expectedMessage = "User " + userId + " does not have the permission " + permissionName;
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }
    
    @Then("^I get message: Data provided could not be parsed as a date$")
    public void iGetErrorResponseForInvalidDateFormat() {
        Response response = Serenity.sessionVariableCalled("response");
        String expectedMessage ="Data provided could not be parsed as a date";
        String actualMessage = response.body().jsonPath().get("message");
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }
    
    @Then("^I receive the same permission id in the delete response$")
    public void iReceiveTheSamePermissionIdAfterDeleting() {
        Response response = Serenity.sessionVariableCalled("response");
        int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
        int actualPermissionId = response.body().jsonPath().get("data.permission_id");
        assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
    }
    
	@Then("^I receive the same permission id after update request$")
	public void iReceiveTheSamePermissionIdAfterUpdateRequest() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
		int actualPermissionId = response.body().jsonPath().get("data.id");
		assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
	}

	@Then("^Verify the expected permission data with actual permission data$")
	public void verifyTheExpectedPermissionDataWithActualPermissionData() {
		Response response = Serenity.sessionVariableCalled("response");
		String expectedPermissionName = Serenity.sessionVariableCalled("expPermissionName");
		String expectedPermissionType = Serenity.sessionVariableCalled("expPermissionType");
		String actualPermissionName = response.body().jsonPath().get("data.name");
		String actualPermissionType = response.body().jsonPath().get("data.permission_type");
		assertThat(expectedPermissionName).isEqualTo(actualPermissionName);
		assertThat(expectedPermissionType).isEqualTo(actualPermissionType);
	}
	
	@Then("^I get the same permission details which was assigned$")
	public void iGetTheSamePermissionDetailsWhichWasAssigned() {
		Response response = Serenity.sessionVariableCalled("response");
		String expectedPermissionName = Serenity.sessionVariableCalled("expectedPermissionName");
		String expectedPermissionType = Serenity.sessionVariableCalled("expectedPermissionType");
		int expectedPermissionId = Serenity.sessionVariableCalled("expectedPermissionId");
		String actualPermissionName = response.body().jsonPath().get("data[0].name");
		String actualPermissionType = response.body().jsonPath().get("data[0].permission_type");
		int actualPermissionId = response.body().jsonPath().get("data[0].permission_id");
		assertThat(expectedPermissionName).isEqualTo(actualPermissionName);
		assertThat(expectedPermissionType).isEqualTo(actualPermissionType);
		assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
	}
	
	@Then("^I verify the actual user id with expected user id$")
	public void iVerifyTheActualUserIdWithExpectedUserId() {
		Response patchResponse = Serenity.sessionVariableCalled("response");
		int expectedUserId = Serenity.sessionVariableCalled("userId");
		int actualUserId = patchResponse.body().jsonPath().get("data.user_id");
		assertThat(actualUserId).isEqualTo(expectedUserId);
	}
	
	@Then("^Verify updated user data$")
	public void verifyUpdatedUserData() {
		int userId = Serenity.sessionVariableCalled("userId");
		String expectedStreetAddress1 = Serenity.sessionVariableCalled("streetAddress1");
		String expectedStreetAddress2 = Serenity.sessionVariableCalled("streetAddress2");
		String expectedCity = Serenity.sessionVariableCalled("city");
		String expectedLocality = Serenity.sessionVariableCalled("locality");
		String expectedProvince = Serenity.sessionVariableCalled("province");
		String expectedPostalCode = Serenity.sessionVariableCalled("postalCode");
		String expectedCountryCode = Serenity.sessionVariableCalled("countryCode");
		Response response = SerenityRest
	                .given()
	                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
	                    .header("Content-Type", "application/json")
	                .when()
	                    .get(defaultUrl() + "/users/" + userId);
		String actualStreetAddress1=response.body().jsonPath().get("data.street_address_foo.street_address1");
        String actualstreetAddress2=response.body().jsonPath().get("data.street_address_foo.street_address2");
        String actualCity=response.body().jsonPath().get("data.street_address_foo.city");
        String actualLocality=response.body().jsonPath().get("data.street_address_foo.locality");
        String actualProvince=response.body().jsonPath().get("data.street_address_foo.province");
        String actualPostalCode=response.body().jsonPath().get("data.street_address_foo.postal_code");
        String actualCountryCode=response.body().jsonPath().get("data.street_address_foo.country_code");
        assertThat(actualStreetAddress1).isEqualTo(expectedStreetAddress1);
        assertThat(actualstreetAddress2).isEqualTo(expectedStreetAddress2);
        assertThat(actualCity).isEqualTo(expectedCity);
        assertThat(actualLocality).isEqualTo(expectedLocality);
        assertThat(actualProvince).isEqualTo(expectedProvince);
        assertThat(actualPostalCode).isEqualTo(expectedPostalCode);
        assertThat(actualCountryCode).isEqualTo(expectedCountryCode);
	}
	
	@Then("^I get the updated user id and assigned permission id$")
    public void iGetTheRespectiveUseridAndPermissionId() {
        Response postResponse = Serenity.sessionVariableCalled("response");
        int expectedUserId = Serenity.sessionVariableCalled("userId");
        int expectedPermissionId = Serenity.sessionVariableCalled("expectedPermissionId");
        int actualUserId = postResponse.body().jsonPath().get("data.user_id");
        int actualPermissionId = postResponse.body().jsonPath().get("data.permissions[0]");
        assertThat(actualUserId).isEqualTo(expectedUserId);
        assertThat(actualPermissionId).isEqualTo(expectedPermissionId);
    }
	
	 @Then("^Verify the expected user id with actual user id$")
	    public void verifyTheActualUserIdWithExpectedUserId() {
	        Response response = Serenity.sessionVariableCalled("response");
	        int expectedUserId =Serenity.sessionVariableCalled("userId");
	        int actualUserId = response.body().jsonPath().get("data.id");
	        assertThat(actualUserId).isEqualTo(expectedUserId);
	    }
	 
	@Then("^Verify the existing permission id with returned permission id$")
	public void verifyPermissionIdWithExpectedPermissionId() {
		int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
		Response response = Serenity.sessionVariableCalled("response");
		int actualPermissionId = response.body().jsonPath().get("data.permissions[0]");
		assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
	}

	@Then("^Verify the existing  package id with returned package id$")
	public void verifyTheActualPackageIdWithExpectedPackageId() {
		Response patchResponse = Serenity.sessionVariableCalled("response");
		int expectedPackageId = Serenity.sessionVariableCalled("packageId");
		int actualPackageId = patchResponse.body().jsonPath().get("data.package_id");
		assertThat(actualPackageId).isEqualTo(expectedPackageId);
	}
	
	@Then("^I get message: Cannot set expiration with provided data$")
    public void iGetErrorResponseForInvalidExxpirationDate() {
        Response response = Serenity.sessionVariableCalled("response");
        String expectedMessage ="Cannot set expiration with provided data";
        String actualMessage = response.body().jsonPath().get("message");
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I receive the same package id after update request$")
	public void iReceiveTheSamePackageIdAfterUpdateRequest() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedPackageId = Serenity.sessionVariableCalled("packageId");
		int actualPackageId = response.body().jsonPath().get("data.package_id");
		assertThat(expectedPackageId).isEqualTo(actualPackageId);
	}

	@Then("^I receive the same package id after delete request$")
	public void iReceiveTheSamePackageIdAfterDeleteRequest() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedPackageId = Serenity.sessionVariableCalled("packageId");
		int actualPackageId = response.body().jsonPath().get("data.package_id");
		assertThat(expectedPackageId).isEqualTo(actualPackageId);
	}
	
	@Then("^I receive the same permission id after update duration request$")
	public void iReceiveTheSamePackageIdAfterUpdateDurationRequest() {
		Response response = Serenity.sessionVariableCalled("response");
		int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
		int actualPermissionId = response.body().jsonPath().get("data.id");
		assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
	}
	
	@Then("^I get message: Password invalid$")
	public void iGetAMessagePasswordInvalid() {
		Response response = Serenity.sessionVariableCalled("response");
		String returnedMessage = response.body().jsonPath().get("message");
		String expectedMessage ="Password invalid";
		assertThat(returnedMessage).isEqualTo(expectedMessage);
	}
	
	@Then("^I get the deleted permission id$")
    public void iGetTheDeletedPermissionId() {
        Response response = Serenity.sessionVariableCalled("response");
        int expectedPermissionId = Serenity.sessionVariableCalled("permissionId");
        int actualPermissionId = response.body().jsonPath().get("data.id");
        assertThat(expectedPermissionId).isEqualTo(actualPermissionId);
    }
	
	@Then("^I get message: Duration in days must be an integer$")
	public void iGetTheMessageDurationInDaysMustBeAnInteger() {
		Response response = Serenity.sessionVariableCalled("response");
		String returnedMessage = response.body().jsonPath().get("message");
		String expectedMessage = "Duration in days must be an integer";
		assertThat(returnedMessage).isEqualTo(expectedMessage);
	}
	
	@Then("^I get message: Permission is already assigned to the package$")
	public void iGetTheValidErrorMessageForThePermission() {
		String expectedPermissionName= Serenity.sessionVariableCalled("permissionName");
		int expectedPackageId=Serenity.sessionVariableCalled("packageId");
		Response response = Serenity.sessionVariableCalled("response");
		String expectedMessage= "Permission " + expectedPermissionName + " is already associated to package " + expectedPackageId;
		String actualMessage = response.body().jsonPath().get("message");
		assertThat(actualMessage).isEqualTo(expectedMessage);
	}

	@Then("^I get a message: Package id cannot be found$")
    public void iGetAMessageForInvalidPackageId() {
        Response response = Serenity.sessionVariableCalled("response");
        int packageId= Serenity.sessionVariableCalled("packageId");
        String returnedMessage = response.body().jsonPath().get("message");
        String expectedMessage = "Package " + packageId + " cannot be found";
        assertThat(returnedMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I get a message: Package cannot be found$")
    public void iGetAMessagePackageCannotBeFound() {
       Response response = Serenity.sessionVariableCalled("response");
       int packageId= Serenity.sessionVariableCalled("packageId");
       String returnedMessage = response.body().jsonPath().get("message");
       String expectedMessage = "Package " + packageId + " cannot be found";
       assertThat(returnedMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I get message: Package name cannot be blank/Package already exists$")
    public void iGetErrorResponseForCreatingPackageWithBlankSpace() {
        Response response = Serenity.sessionVariableCalled("response");
        String expectedMessage ="Package name cannot be blank/Package already exists";
        String actualMessage = response.body().jsonPath().get("message");
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I get message: A suspension reason is required$")
    public void iGetMessageASsuspensionReasonIsRequired() {
        Response response = Serenity.sessionVariableCalled("response");
        String expectedMessage= "A suspension reason is required";
        String actualMessage = response.body().jsonPath().get("message");
        assertThat(actualMessage).isEqualTo(expectedMessage);
    }
	
	@Then("^I get error message: Permissions must be an array$")
    public void iGetErrorMessageForIncorrectPermissionArray() {
        Response response = Serenity.sessionVariableCalled("response");
        String acutualMessage = response.body().jsonPath().get("message");
        assertThat(acutualMessage).isEqualTo("Permissions must be an array");
    }
	
	@Then("^I get message: Cannot delete permission of the user because it is already assigned$")
	public void iGetErrorMessageCannotDeletePermissionOfTheUserBecauseItIsAlreadyAssigned() {
		Response response = Serenity.sessionVariableCalled("response");
		String permissionName = Serenity.sessionVariableCalled("permissionName");
		String actualMessage = response.body().jsonPath().get("message");
		String expectedMessage = "Cannot delete permission " + permissionName + " because it is already assigned.";
        assertThat(actualMessage).isEqualTo(expectedMessage);
	}
	
	@Then("^I get message: Cannot set expiration with provided data$")
    void iGetErrorMessageCannotSetExpirationWithProvidedData() {
        Response response = Serenity.sessionVariableCalled("response");
        String data = response.body().jsonPath().get("message");
        assertThat(data).isEqualTo("Cannot set expiration with provided data");
    }
}