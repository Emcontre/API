package ascendlearning.steps;

import ascendlearning.data.CommonData;
import ascendlearning.data.SiteLicenseData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import java.util.HashMap;

public class SiteLicenseAPISteps extends CustomScenarioSteps {
	@Given("^I have a valid site license data$")
	public void iHaveAValidSiteLicenseData() {
		HashMap<String, String> siteLicense = SiteLicenseData.validSiteLicenseData();
		Serenity.setSessionVariable("siteLicense").to(siteLicense);
	}
	
	@Given("^I have a valid site license$")
	public void iHaveAValidSiteLicense() {
		HashMap<String, String> postBody = SiteLicenseData.validSiteLicenseData();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/site_licenses");
		int createdSiteLicenseID = response.body().jsonPath().get("data.site_license_id");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("siteLicenseId").to(createdSiteLicenseID);		
	}
	
	@Given("^I have an invalid site license id$")
	public void iHaveAnInvalidSiteLicenseId() {
		int invalidSiteLicenseId = CommonData.INVALIDSITELICENSEID;
		Serenity.setSessionVariable("siteLicenseId").to(invalidSiteLicenseId);		
	}

	@When("^I create a site license$")
	public void iCreateASiteLicense() {
		HashMap<String, String> postBody = Serenity.sessionVariableCalled("siteLicense");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/site_licenses");
		Serenity.setSessionVariable("response").to(response);
	}

	@When("^I delete the site license$")
	public void iDeleteASiteLicense() {
		Response siteLicense = Serenity.sessionVariableCalled("response");
		int siteLicenseID = siteLicense.body().jsonPath().get("data.site_license_id");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/site_licenses" + "/" + siteLicenseID);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("createdSitelicenseID").to(siteLicenseID);
	}

	@When("^I query the license details$")
	public void iQueryASiteLicense() {
		int siteLicenseId = Serenity.sessionVariableCalled("siteLicenseId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/site_licenses" + "/" + siteLicenseId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("siteLicenseId").to(siteLicenseId);
	}

	@When("^I update the license details$")
	public void iUpdateASiteLicense() {
		int siteLicenseId = Serenity.sessionVariableCalled("siteLicenseId");
		HashMap<String, String> postBody = SiteLicenseData.updateRedemptionLimit();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.patch(defaultUrl() + "/site_licenses" + "/" + siteLicenseId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("siteLicenseId").to(siteLicenseId);
	}	
}
