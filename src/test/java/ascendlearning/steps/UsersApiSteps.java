package ascendlearning.steps;

import ascendlearning.data.CommonData;
import ascendlearning.data.UsersData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import java.util.HashMap;



public class UsersApiSteps extends CustomScenarioSteps {

	@Given("^I have valid user details$")
	public void iHaveValidUserDetails() {
		HashMap<String, String> newuser = UsersData.randomActiveUser();
		Serenity.setSessionVariable("user").to(newuser);
	}

	@Given("^I have an existing user$")
	public void iHaveAnExistingUser() {
		HashMap<String, String> postBody = UsersData.randomActiveUser();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users");
		String createdUserEmailId = postBody.get("email");
		int createdUserUAID = response.body().jsonPath().get("data.user_id");
		String createdUserUserName = postBody.get("username");
		Serenity.setSessionVariable("createdUserEmailId").to(createdUserEmailId);
		Serenity.setSessionVariable("userUAId").to(createdUserUAID);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("createdUserUserName").to(createdUserUserName);
	}
	
	@Given("^I have an invalid user id$")
	public void iHaveAnInvalidUserId() {
		int invalidUserId = CommonData.INVALIDUSERID;
		Serenity.setSessionVariable("userUAId").to(invalidUserId);
	}
	
	@Given("^I have an existing suspended user$")
    public void iHaveAnExistingSuspendedUser() {
        HashMap<String, String> postBody = UsersData.randomActiveUser();
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/users");
        int createdUserId = response.body().jsonPath().get("data.user_id");
        HashMap<String, Object> patchBody = UsersData.suspendExistingUser(createdUserId);
        Response patchResponse = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(patchBody)
                .when()
                    .patch(defaultUrl() + "/users/suspend");      
        Serenity.setSessionVariable("patchResponse").to(patchResponse);
        Serenity.setSessionVariable("userUAId").to(createdUserId);
    }

	@When("^I create the user$")
	public void iCreateTheUser() {
		HashMap<String, String> postBody = Serenity.sessionVariableCalled("user");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users");
		Serenity.setSessionVariable("response").to(response);
	}

	@When("^I query the user details by UAID$")
	public void iQueryTheUserDeatilsByUAID() {
		int userUAId = Serenity.sessionVariableCalled("userUAId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/users/" + userUAId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userUAId").to(userUAId);
	}
	
	@When("^I query the user details by email id$")
	public void iQueryTheUserDetailsByEmailId() {
		String createdUserEmailId = Serenity.sessionVariableCalled("createdUserEmailId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.get(defaultUrl() + "/users/" + createdUserEmailId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("createdUserEmailId").to(createdUserEmailId);
	}
	
	@When("^I send a request to delete the user$")
	public void iSendARequestToDeleteTheUser() {
		int userUAId = Serenity.sessionVariableCalled("userUAId");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
				.when()
					.delete(defaultUrl() + "/users/" + userUAId);
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userUAId").to(userUAId);
	}
	
	@When("^I update first name and last name$")
	public void iUpdateFirstNameAndLastName() {
		Response createdUser = Serenity.sessionVariableCalled("response");
		int createdUserId = createdUser.body().jsonPath().get("data.user_id");
		HashMap<String, String> randomFirstAndLastName = UsersData.randomNewFirstAndLastNameForUser();
		String newFirstName = randomFirstAndLastName.get("first_name");
		String newLastName = randomFirstAndLastName.get("last_name");
    	Response response = SerenityRest
    			.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(randomFirstAndLastName)
				.when()
					.patch(defaultUrl() + "/users/" + createdUserId);       
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("createdUserId").to(createdUserId);
		Serenity.setSessionVariable("newFirstName").to(newFirstName);
		Serenity.setSessionVariable("newLastName").to(newLastName);
	}
	
	@When("^I create a new user with existing user name$")
    public void iCreateANewUserWithExistingUserName() {
        String existingUserName = Serenity.sessionVariableCalled("createdUserUserName");
        HashMap<String, String> postBody = UsersData.randomActiveUser();
        postBody.put("username",existingUserName);
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/users");   
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("existingUserName").to(existingUserName);
    }
	
	@When("^I update the user from active to suspended$")
    public void iUpdateTheUserFromActiveToSuspended() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		HashMap<String, Object> patchBody = UsersData.suspendExistingUser(userId);
		Response response = SerenityRest
				.given()
                	.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                	.header("Content-Type", "application/json")
                	.body(patchBody)
                .when()
                	.patch(defaultUrl() + "/users/suspend");      
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("userUAId").to(userId);
    }
	
	@When("^I update the user from suspended to active$")
    public void iUpdateTheUserFromSuspendedToActive() {
		int suspendedUserId = Serenity.sessionVariableCalled("userUAId");
		HashMap<String, Object> patchBody = UsersData.reinstateExistingUser(suspendedUserId);
        Response response = SerenityRest
        		.given()
                	.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                	.header("Content-Type", "application/json")
                	.body(patchBody)
                .when()
                	.patch(defaultUrl() + "/users/reinstate");      
        Serenity.setSessionVariable("response").to(response);
        Serenity.setSessionVariable("userUAId").to(suspendedUserId);
    }
	
	@When("^I create a new user with existing email id$")
    public void iCreateANewUserWithExistingEmailId() {
       	String existingEmailId = Serenity.sessionVariableCalled("createdUserEmailId");
        HashMap<String, String> postBody = UsersData.randomActiveUser();
        postBody.put("email",existingEmailId);
        Response response = SerenityRest
                .given()
                    .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                    .header("Content-Type", "application/json")
                    .body(postBody)
                .when()
                    .post(defaultUrl() + "/users");
        Serenity.setSessionVariable("response").to(response);
	}
	
	@When("^I send a request to update user details$")
	public void iSendARequestToUpdateUserDetails() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		HashMap<String, Object> postBody = UsersData.updateUserData();
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.patch(defaultUrl() + "/users/" + userId);
		@SuppressWarnings("unchecked")
		HashMap<String, Object> addressDetails = (HashMap<String, Object>) postBody.get("street_address_foo");
		String streetAddress1 = (String) addressDetails.get("street_address1");
		String streetAddress2 = (String) addressDetails.get("street_address2");
		String city = (String) addressDetails.get("city");
		String locality = (String) addressDetails.get("locality");
		String province = (String) addressDetails.get("province");
		String postalCode = (String) addressDetails.get("postal_code");
		String countryCode = (String) addressDetails.get("country_code");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userId").to(userId);
		Serenity.setSessionVariable("streetAddress1").to(streetAddress1);
		Serenity.setSessionVariable("streetAddress2").to(streetAddress2);
		Serenity.setSessionVariable("city").to(city);
		Serenity.setSessionVariable("locality").to(locality);
		Serenity.setSessionVariable("province").to(province);
		Serenity.setSessionVariable("postalCode").to(postalCode);
		Serenity.setSessionVariable("countryCode").to(countryCode);
	}
	
	@When("^I send request to change password$")
	public void iSendRequestToChangeUserPassword() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		String userName = Serenity.sessionVariableCalled("createdUserUserName");
		HashMap<String, String> putBody = UsersData.toGetPasswordChangeToken();
		Response putResponse = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(putBody)
				.when()
					.put(defaultUrl() + "/users/" + userName + "/reset_password");
		String token = putResponse.body().jsonPath().get("data.token");
		HashMap<String, String> patchBody = UsersData.passwordChangeData(token);
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(patchBody)
				.when()
					.patch(defaultUrl() + "/users" + "/change_password");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userId").to(userId);
	}
	
	@When("^I send request to change password with invalid password$")
	public void iSendRequestToChangePasswordWithInvalidPassword() {
		int userId = Serenity.sessionVariableCalled("userUAId");
		String userName = Serenity.sessionVariableCalled("createdUserUserName");
		HashMap<String, String> putBody = UsersData.toGetPasswordChangeToken();
		Response putResponse = SerenityRest
				.given()
				.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
				.header("Content-Type", "application/json")
				.body(putBody)
				.when()
				.put(defaultUrl() + "/users/" + userName + "/reset_password");
		String token = putResponse.body().jsonPath().get("data.token");
		HashMap<String, String> patchBody = UsersData.invalidPasswordChangeData(token);
		Response response = SerenityRest
				.given()
				.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
				.header("Content-Type", "application/json")
				.body(patchBody)
				.when()
				.patch(defaultUrl() + "/users" + "/change_password");
		Serenity.setSessionVariable("response").to(response);
		Serenity.setSessionVariable("userId").to(userId);
	}
	
	@When("^I send request to suspend the user with invalid suspension reason$")
    public void iSendRequestToSuspendTheUserWithInvalidSuspensionReason() {
        int userId = Serenity.sessionVariableCalled("userUAId");
        HashMap<String, String> patchBody = UsersData.invalidSuspensionUserData(userId);
        Response response = SerenityRest
                .given()
                .header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
                .header("Content-Type", "application/json")
                .body(patchBody)
                .when()
                .patch(defaultUrl() + "/users" + "/suspend");
        Serenity.setSessionVariable("response").to(response);
    }
}