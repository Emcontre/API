package ascendlearning.steps;

import ascendlearning.data.StreetAddressData;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;

import java.util.HashMap;

public class StreetAddressAPISteps extends CustomScenarioSteps {

	@Given("^I have a valid street address details$")
	public void aValidStreetAddress() {
		HashMap<String, String> validStreetAddressDetails = StreetAddressData.validStreetAddressDetails();
		Serenity.setSessionVariable("streetAddressDetails").to(validStreetAddressDetails);
	}

	@Given("^I have a valid address with street address and postal code$")
	public void aValidStreetAddressWithPostalCode() {
		HashMap<String, String> validStreetAddressWithPostalCode = StreetAddressData.validStreetAddressWithPostalCode();
		Serenity.setSessionVariable("streetAddressDetails").to(validStreetAddressWithPostalCode);
	}

	@Given("^I have a street address with invalid postal code$")
	public void aStreetAddressWithInvalidPostalCode() {
		HashMap<String, String> streetAddressWithInvalidPostalCode = StreetAddressData.streetAddressWithInvalidPostalCode();
		Serenity.setSessionVariable("streetAddressDetails").to(streetAddressWithInvalidPostalCode);
	}
	
	@Given("^I have a street address with invalid country code$")
    public void aStreetAddressWithInvalidCountryCode() {
        HashMap<String, String> streetAddressWithInvalidCountryCode = StreetAddressData.streetAddressWithInvalidCountryCode();
        Serenity.setSessionVariable("streetAddressDetails").to(streetAddressWithInvalidCountryCode);
    }
	
	@Given("^I have a street address without country code$")
	public void aStreetAddressWithoutCountryCode() {
		HashMap<String, String> streetAddressWithOutCountryCode = StreetAddressData.streetAddressWithOutCountryCode();
		Serenity.setSessionVariable("streetAddressDetails").to(streetAddressWithOutCountryCode);
	}
	
	@Given("^I have street address details without street address1$")
	public void iHaveStreetAddressDetailsWithoutStreetAddress1() {
		HashMap<String, String> invalidStreetAddressDetails = StreetAddressData.streetAddressDetailsWithoutStreetAddress1();
		Serenity.setSessionVariable("streetAddressDetails").to(invalidStreetAddressDetails);
	}

	@When("^I verify the street address$")
	public void iVerifyTheStreetAddress() {
		HashMap<String, String> postBody = Serenity.sessionVariableCalled("streetAddressDetails");
		Response response = SerenityRest
				.given()
					.header("Authorization", "Token token=changeth-isto-some-thin-ghexadecimal")
					.header("Content-Type", "application/json")
					.body(postBody)
				.when()
					.post(defaultUrl() + "/street_addresses/verify");
		Serenity.setSessionVariable("response").to(response);
	}
}